//
//  main.c
//  Fibonacci
//
//  Created by Richard Stewing on 13.08.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//Calculates the Fibonacci number 

#include <stdio.h>
//outer functions
//gets the user input
void input( void);
//calculates an array with the number of fibonacci numbers until the index of the number you passed in
void calc(int n);
//quits or let the user input a new number to start again
void done(void);

int main(int argc, const char * argv[])
{

    // insert code here...
    //Welcomes and tells you what is going on
    printf(" Welcome to the Fibonacci world!!! \n");
    //kicks of the input
    input();
    //quits at the far end 
    return 0;
    
}

//input generater
void input(void){
    //asks for a number
    printf(" How many numbers out of the order you want to see?? Which Index should the last number have strating with 0 ??\n");
    //allocates memory
    int n = 12;
    //reads from the user
    scanf("%d", &n);
    //checks when the index <=0 zero and let's the user start of if it is
    if(n<=0)
        done();
    //if the number is bigger than 45 it let you start other because memory is not high enough
    else if (n>50)
        done();
    else{
        //calcs (only if all above is fall 
        calc(n);
    }
}

//calculates
void calc(int n ){
    // intits 
    long long arr[n];
    //fills in because it is always zero and always needed
    arr[0] = 0;
    //for loop to get the number for all indexes
    for(int a = 1; a<n ; a++){
        //first number and second and result since the result is always the sum of the other ones 
        long long first = 0;
        long long second = 1;
        long long result = 1;
        for (int i = 1; i<a; i++) {
            result = first + second;
            first = second;
            second = result;
        }
        //passes in in the array
        arr[a] = result;
    }
    //prints out for the screen 
    for(int c = 0; c<n; c++){
        
        printf("\n%lld\n", arr[c]);
    }
    //fire up done
    done();
    
    
}
//says good by or lets the user start agin 
void done(void){
    printf("If you want to beginn again <1>  if not  <2>\n");
    int again = 2;
    scanf("%d", &again);
    if(again == 1){
         input();
    }else{
        printf("i quit");
    }
    
}



